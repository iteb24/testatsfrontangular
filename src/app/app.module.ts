// Modules
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Services
import { AuthService } from './services/auth/auth.service';
import { UserService } from './services/user/user.service';

import { ProductService } from './services/product/product.service';
// Pipes
import { FilterPipe } from './pipes/filter.pipe';
// Components
import { AppComponent } from './components/index/app.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent, homeChildRoutes } from './components/home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { ProductDetailsComponent } from './components/products/details/product-details.component';
import { ProductListComponent } from './components/products/list/product-list.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
		HomeComponent,
		FilterPipe,
		ProductDetailsComponent,
		ProductListComponent
		
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
		RouterModule,
		FormsModule,
		ReactiveFormsModule,
		BrowserAnimationsModule,
		
  ],
  providers: [AuthService, UserService,ProductService],
  bootstrap: [AppComponent]
})
export class AppModule { }
