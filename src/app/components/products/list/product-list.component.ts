import { Component, OnInit } from '@angular/core';


// Services
import { ProductService } from '../../../services/product/product.service';
import { routerTransition } from '../../../services/config/config.service';


@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
  animations: [routerTransition()],
	host: { '[@routerTransition]': '' }
})
export class ProductListComponent implements OnInit {

  productList: any;
	productListData: any;

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.getProductList();
  }
  getProductList() {
		const productList = this.productService.getAllProducts();
		this.success(productList);
	}

  success(data) {
		this.productListData = data.data;
		for (let i = 0; i < this.productListData.length; i++) {
			this.productListData[i].name = this.productListData[i].category + ' ' + this.productListData[i].productName;
		}
	}
}
