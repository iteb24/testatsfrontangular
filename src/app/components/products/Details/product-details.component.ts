import { Component, OnInit } from '@angular/core';

import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';


// Services
import { ProductService } from '../../../services/product/product.service';
import { routerTransition } from '../../../services/config/config.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  index: any;
	productDetail: any;

  constructor(private router: Router, private route: ActivatedRoute, private productService: ProductService) { 

    this.route.params.subscribe(params => {
			this.index = params['id'];
			if (this.index && this.index != null && this.index !== undefined) {
				this.getProductDetails(this.index);
			}
		});
  }

  ngOnInit() {
  }

  getProductDetails(index: number) {
		const getProductDetail = this.productService.getProductDetails(index);
		if (getProductDetail) {
			this.productDetail = getProductDetail.hotelData;
		
		}
	}

}
