

import { Component } from '@angular/core';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})


export class AppComponent {
	title = 'Mini App';



	
	productList = [
		{
            id:"5fa562ca59719c29980c8860",
            color:"fuchsia",
            category:"Outdoors",
            productName:"Sleek Concrete Soap",
			price:118,
			description:"Automated encompassing Graphic Interface",
            tag:"Handmade",
            productMaterial:"Plastic",
            imageUrl:"http://lorempixel.com/640/480/technics",
            createdAt:"2020-11-06T13:08:53.485Z",
            reviews:
            [ 
                {
                 rating:2,
                 content:"Ut ut nihil. Id libero aperiam accusantium temporibus sit est distinctio id error. Animi corrupti officiis eligendi. Laborum provident quo cum amet ullam."},{"rating":2,"content":"Libero aliquid asperiores est molestiae et eum aut sed et. Aut reprehenderit voluptas repudiandae est dolor reprehenderit id. Deserunt nihil ipsa eum itaque."
                },
                {
                 rating:2,
                 content:"Voluptas dolorem et. Est vitae dolores aut debitis eos. Ipsa et repellat excepturi vel. Omnis minus sequi velit et at veritatis vel. Vel voluptatem qui recusandae error quia ut velit. Iste explicabo saepe dolor quo odio voluptatem veniam molestiae."
                },
                {
                    rating:4,
                    content:"Officia blanditiis necessitatibus corrupti voluptatem non perferendis molestias velit. Est sunt saepe enim quo adipisci aut consequatur. Libero voluptas voluptatum est necessitatibus. Eveniet nihil deleniti perferendis ducimus consequatur iure quas velit accusantium."
                    
                },
                {
                    rating:0,
                    content:"Sequi maxime vel fuga provident dolores recusandae quia delectus. A quam qui est. Occaecati et expedita aliquid qui. Blanditiis quos asperiores facere nostrum laboriosam reprehenderit in qui."
                },
                {
                    rating:4,
                    content:"Totam alias eos earum dolorem quis. Dignissimos ullam eveniet est in rerum non est. Dolor dolor dolores a voluptas. Officia recusandae maiores."
                }]
                }
                ,{
                    id:"5fa566ddbaa350299cdd65ce",
                    color:"red",
                    category:"Beauty",
                    productName:"Rustic Wooden Fish",
                    price:980,
                    description:"Synergistic responsive workforce",
                    tag:"Rustic",
                    productMaterial:"Frozen",
                    imageUrl:"http://lorempixel.com/640/480/technics",
                    createdAt:"2020-11-06T13:08:53.485Z",
                    reviews:
                    [
                        {
                            rating:2,
                            content:"Esse ut est. Ut assumenda vitae placeat sit blanditiis amet. Neque voluptate temporibus harum ut repudiandae iste aut."
                        },
                        {
                            rating:3,
                            content:"Eveniet voluptas in illum consequatur alias fuga enim nobis quaerat. Voluptas atque in alias earum modi autem blanditiis et corrupti. Optio cumque nesciunt. Corporis aut qui voluptas veritatis maxime eos excepturi voluptatem. Id facilis qui distinctio enim iste corrupti eos molestiae."
                        },
                        {
                            rating:0,
                            content:"Eum cum inventore ut pariatur. Ratione voluptas voluptatem aut accusantium possimus voluptatem. Iste culpa voluptate dolorum necessitatibus rem."
                        },
                        {
                            rating:0,
                            content:"Delectus excepturi hic quidem possimus incidunt reiciendis. Eos molestiae dolore. Facilis mollitia dolorem illo esse perspiciatis corporis. Autem minima fugit."
                        },
                        {
                            rating:0,
                            content:"Aliquid possimus ut voluptatem in. Quidem quo modi qui est voluptates perferendis nihil est. Exercitationem nostrum deserunt exercitationem tempora ut. Et ut veritatis. Et aut quam ipsam sed iure. Est quisquam eligendi neque molestiae at alias quia."
                        },
                        {
                            rating:4,
                            content:"Placeat atque molestiae aut. Rerum et eos occaecati praesentium repudiandae similique numquam et. Adipisci non et velit animi. Molestiae molestiae perferendis sed ad consequatur illum. Sapiente eum ut."
                        }
                    ]}

	
	];

	constructor() {
		
		localStorage.setItem('products', JSON.stringify(this.productList));
	}
}


