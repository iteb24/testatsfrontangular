

 import { Component, OnInit } from '@angular/core';
 import { RouterModule, Routes ,Router} from '@angular/router';


 // Components
 import { ProductListComponent } from '../products/list/product-list.component';
 import { ProductDetailsComponent } from '../products/details/product-details.component';
 // Services
 import { routerTransition } from '../../services/config/config.service';

 @Component({
 	selector: 'app-home',
 	templateUrl: './home.component.html',
 	styleUrls: ['./home.component.css', './home.component.scss', 'navbar.component.scss'],
 	animations: [routerTransition()],
 	host: {'[@routerTransition]': ''}
 })


 export class HomeComponent implements OnInit {
 	active:string;
 	constructor(private router: Router) {
 	
 		this.router.events.subscribe((val) => {
 			this.routeChanged(val);
 		});
 	}

 	ngOnInit() {
 	}

 	
 	routeChanged(val){
 		this.active = val.url;
 	}

 	
 	logOut(){
 		
 		localStorage.removeItem('userData');
 		this.router.navigate(['/login']);
 	}
 }



 export const homeChildRoutes : Routes = [
 {
 	path: 'products',
 	component: ProductListComponent
 },
 
 {
 	path: 'product/:id',
 	component: ProductDetailsComponent
 }
 ];


