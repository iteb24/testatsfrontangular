export class Products {

    color: string;
    category: string;
    productName: string;
    price: string;
    description:string;
    tag:string;
    productMaterial:string;
    imageUrl:string;
    createdAt:Date;
    reviews:string;

}