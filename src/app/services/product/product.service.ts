import { Injectable } from '@angular/core';

@Injectable()
export class ProductService {

  constructor() { }


    
  getAllProducts() {
    let productList: any;
    if (localStorage.getItem('products') && localStorage.getItem('products') !== '') {
      productList = {
        code: 200,
        message: 'products List Fetched Successfully',
        data: JSON.parse(localStorage.getItem('products'))
      };
    } else {
      productList = {
        code: 200,
        message: 'Products List Fetched Successfully',
        data: JSON.parse(localStorage.getItem('poducts'))
      };
    }
    return productList;
  }


  getProductDetails(index: number) {
    const productList = JSON.parse(localStorage.getItem('products'));

    const returnData = {
      code: 200,
      message: 'Product Details Fetched',
      hotelData: productList[index]
    };

    return returnData;
  }


}
